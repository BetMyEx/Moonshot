//
//  Astronaut.swift
//  Moonshot
//
//  Created by Admin on 02.02.2022.
//

import Foundation

struct Astronaut: Codable, Identifiable {
    let id: String
    let name: String
    let description: String
}
