//
//  ContentView.swift
//  Moonshot
//
//  Created by Admin on 01.02.2022.
//

import SwiftUI

struct ContentView: View {
    let astronauts: [String: Astronaut] = Bundle.main.decode("astronauts.json")
    let missions: [Mission] = Bundle.main.decode("missions.json")
    @State var gridLayout = true
    
    
    //    let columns = [
    //        GridItem(.adaptive(minimum: 150))
    //    ]
    var body: some View {
        NavigationView{
            ScrollView{
                GridLayout(missions: missions, astronauts: astronauts, gridWidthChanger: gridLayout)
//                if gridLayout {
//                    GridLayout(missions: missions, astronauts: astronauts, gridWidthChanger: gridLayout)
//                } else {
//                    ListLayout(missions: missions, astronauts: astronauts)
//                }
            }
            .navigationTitle("Moonshot")
            .background(.darkBackground)
            .preferredColorScheme(.dark)
            .toolbar {
                Button(action: {
                    withAnimation {
                        gridLayout.toggle()
                    }
                }) {
                    let image = gridLayout ? "list.dash" : "square.grid.2x2"
                    Label("Change layout", systemImage: image)
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
