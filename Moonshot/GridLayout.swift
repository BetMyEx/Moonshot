//
//  GridLayout.swift
//  Moonshot
//
//  Created by Admin on 16.02.2022.
//

import SwiftUI

struct GridLayout: View {
    
    let missions: [Mission]
    let astronauts: [String: Astronaut]
    let gridWidthChanger: Bool
    
    var columns = [
        GridItem(.adaptive(minimum: 150))
    ]
    
    var columns2 = [
        GridItem(.adaptive(minimum: UIScreen.main.bounds.width))
    ]
    
    
    var body: some View {
        LazyVGrid(columns: gridWidthChanger ? columns : columns2){
            ForEach(missions) { mission in
                NavigationLink {
                    MissionView(mission: mission, astronauts: astronauts)
                } label: {
                    VStack{
                        Image(mission.image)
                            .resizable()
                            .scaledToFit()
                            .frame(width: 100, height: 100)
                            .padding()
                        
                        VStack {
                            
                            Text(mission.displayName)
                                .font(.headline)
                                .foregroundColor(.white)
                            
                            Text(mission.formattedLaunchDate)
                                .font(.caption)
                                .foregroundColor(.white.opacity(0.5))
                        }
                        .padding(.vertical)
                        .frame(maxWidth: .infinity)
                        .background(.lightBackground)
                    }
                    .clipShape(RoundedRectangle(cornerRadius: 10))
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(.lightBackground)
                    )
                }
            }
        }
        .padding([.horizontal, .bottom])
    }
}

struct GridLayout_Previews: PreviewProvider {
    static let astronauts: [String: Astronaut] = Bundle.main.decode("astronauts.json")
    static let missions: [Mission] = Bundle.main.decode("missions.json")
    
    static var previews: some View {
        GridLayout(missions: missions, astronauts: astronauts, gridWidthChanger: false)
    }
}
