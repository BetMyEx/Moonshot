//
//  MoonshotApp.swift
//  Moonshot
//
//  Created by Admin on 01.02.2022.
//

import SwiftUI

@main
struct MoonshotApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
